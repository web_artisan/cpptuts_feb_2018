#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <array>

using namespace std;

array <int, 5> modifyArr(array <int, 5> helloArr){
	for (int i = 0; i < helloArr.size(); ++i)
	{
		helloArr[i] += 5;
	}
	// helloArr[0] += 5;
	return helloArr;
}


int main(){
	/*array <int, 5> testArr {1,2,3,4,5};

	testArr[0] += 2;

	cout << testArr[0] << endl;*/

	array <int , 5> arrayForMod {1,2,3,4,5};

	auto mod = modifyArr(arrayForMod);

	for(int x : mod){
		cout << x << endl;
	}

	cout << "=========================" << endl;

	array<string, 3> strArr {"Ivan", "Bate Venci", "Dankata"};
	strArr.fill("Hazaikata");

	cout << strArr.back() << endl;
	// cout << modifyArr(arrayForMod)[0] << endl;

	return 0;
}