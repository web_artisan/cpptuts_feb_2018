#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <bitset>
#include <cstdint>

using namespace std;

const int secondsInMinute = 60;

int main(){

	signed long int x;

	cout << sizeof(x) * 1024 << endl;

	bitset<32> z = 111;

	cout << z << endl;

	cout << INT8_MIN << endl;

	char symbol = 'a';

	char sameSymbol = 'b' - 1;

	cout << sameSymbol << endl;

	bool negative(false);

	bool negative2(-13);

	cout << negative2 << endl;

	int i = 97;
	char a = i;

	int num = 5l;
	cout << num << endl;

	cout << "================================================" << endl;

	double value1 = 5 * 5 / 2.f, value2 = 5 * 5 / 2;

	cout << "value 1 : " << value1 << endl;
	cout << "value 2 : " << value2 << endl;

	if (value1 > value2) {
	    cout << "value1 is larger" << endl;
	} else {
	    cout << "value2 is larger" << endl;
	}




	// secondsInMinute = 13;
    // cout << secondsInMinute << endl;

	/*string name {"Ivan"};
	string profession ("Web Developer moving towards real C++ programming");
	cout << name << endl;
	cout << profession << endl;*/
	
	
    

	/*std::cout << "Hello World" << std::endl;

	char a = 'A';*/

	// char16_t wcs[] = u"zß水🍌"; // or "z\u00df\u6c34\U0001f34c"
	
	/*char16_t wcs[] = u"ванката";

	cout << wcs << endl;*/
    /*size_t wcs_sz = sizeof wcs / sizeof *wcs;

    printf("%zu UTF-16 code units: [ ", wcs_sz);
    for (size_t n = 0; n < wcs_sz; ++n) printf("%#x ", wcs[n]);
    printf("]\n");*/

	// cout << a << endl;

	return 0;
}