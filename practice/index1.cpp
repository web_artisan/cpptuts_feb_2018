#include <iostream>
#include <string>

using namespace std;

void printAlphabet(char first = 'a', char last = 'z'){
	for(char i = first; i < last; ++i){
		cout << i << endl;
	}
}

void swap(int& x, int& y){
	int oldX = x; x = y; y = oldX; 
}

void swapChars(char& a, char& b){
	char oldChar = a; a = b; b = oldChar;
}

int sum(int a, int b){
	return a + b;
}

int sum(int a, int b, int c){
	/*int sum = a + b;
	string result = to_string(a) + to_string(b) + message;*/
	return a + b + c;
}

int main(){

	cout << sum(10, 20, 30) << endl;	

	/*int arr[10];

	cout << "Fifth element of the array BEFORE modifying : " << arr[5] << endl;

	int v;
	cin >> v;

	arr[5] = v;

	cout << "Fifth element of the array AFTER modifying : " << arr[5] << endl;

	cout << "==================================" << endl;


	string text {"random text"};

	cout << text << endl;

	char symbol = '?';

	text.append(symbol);

	cout << text << endl;*/


	/*int a = 300;
	int b = 700;

	cout << "a before swapping : " << a << endl;
	cout << "b before swapping : " << b << endl;

	swap(a, b);

	
	cout << "a after swapping : " << a << endl;
	cout << "b after swapping : " << b << endl;

	char z = 'z';
	char f = 'f';

	cout << "z before swapping : " << z << endl;
	cout << "f before swapping : " << f << endl;

	swapChars(z, f);

	
	cout << "z after swapping : " << z << endl;
	cout << "f after swapping : " << f << endl;

	// printAlphabet('g', 'y');

	int i;
	double d;

	int& r = i;
	double& s = d;

	cout << "Value of i : " << i << endl;
   	cout << "Value of i reference : " << r  << endl;

   	cout << "Value of d : " << d << endl;
   	cout << "Value of d reference : " << s  << endl;*/

	return 0;
}