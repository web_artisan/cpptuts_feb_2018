#include <iostream>
// #include <typeinfo>

using namespace std;

int main(){

	cout << "This is a basic calculator \n";

	int a,b,sum;

	cout << "Enter a random number : " << endl;
	cin >> a;

	cout << "Enter another random number : " << endl;
	cin >> b;

	if(cin.fail()){
		cout << "Error ! Number must be an integer " << endl;
	}else{
		sum = a + b;
		cout << "The sum of the two numbers is : " << sum << endl;
	}

	return 0;
}