#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>

using namespace std;

string appendToStr(string text){
	text.append("Hello World");
	return text;
}

int main(){
	string result = appendToStr("What's up people ");
	cout << result << endl;
	return 0;
}