#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <array>

using namespace std;

static int x = 10;

double movingAverage(int nextNumber);

void printArr(int a[], int size){

	for (int i = 0; i < size; ++i){
		cout << a[i] << endl;
	}
}

/*array <int, 5> modifyArr(int mnojitel, int size){
	array <int, size> testArr;

	for(int x : testArr){

		// testArr[x] += 1;
	}

	return testArr;
}*/

int main(){

	
	/*int arr[5] {11,22,33,44,55};

	for(int number: arr){
		cout << number << endl;
	}*/

	// cout << arr[3] << endl;
	/*int arr[4];

	for (int i = 0; i < sizeof(arr)/sizeof(arr[0]); ++i){
		cin >> arr[i];
	}

	cout << "=======================" << endl;

	for (int i = 0; i < sizeof(arr)/sizeof(arr[0]); ++i){
		cout << arr[i] << endl;
	}*/

	// cout << movingAverage(10) << endl;
	return 0;
}

double movingAverage(int nextNumber) {
    static int count = 0;
    static int sum = 0;
    sum += nextNumber;
    count++;
    return sum / (double)count;
}
