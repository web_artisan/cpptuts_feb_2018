#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <vector>

using namespace std;

vector<int> modifyVector(vector<int>& mod){
	for (int i = 0; i < mod.size(); ++i){
		mod[i] += 10;
	}
	return mod;
}

int main(){
	/*vector<int> testVector {1,2,3,4};

	vector<int> mod = modifyVector(testVector);

	for(int x : mod){
		cout << x << endl;
	}*/

	int arr[2];

	cin >> arr[0] >> arr[1];

	for(int x : arr){
		cout << x << endl;
	}
	return 0;
}