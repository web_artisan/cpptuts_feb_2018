#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <sstream>

using namespace std;

/*
Write a program that reads an integer number N, then reads a line of N integers, 
and prints the minimum and
maximum of those integers.
*/

int main(){

	int x;
	cin >> x;
	int y = 0;

	for (int i = 0; i < x; ++i){
		int z;
		cin >> z;
		if(z > y){
			y = z;
		} 
	}

	cout << y << endl;

	return 0;
}