#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <sstream>

using namespace std;

/*
Write a program that calculates the greatest common divisor (GCD) of given two numbers. 
Hint: you can use the
Euclidean algorithm.
The two integer numbers will be entered on a single line from the console, separated by a single space.
Find and print their GCD.
*/

int main(){

	int M,N;
	cin >> M >> N;

	while (M!=N){ 
		if(M>N){
			M-=N;	
		}else{
			N-=M;
		}
	}

	cout << M << endl;

	return 0;
}