#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>

using namespace std;

int main(){
	int a,b;

	cin >> a >> b;

	if(a > b){
		cout << b << " " << a << endl;
	}else{
		cout << a << " " << b << endl;
	}
	return 0;
}