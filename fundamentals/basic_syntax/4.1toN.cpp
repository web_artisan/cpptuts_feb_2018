#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>

using namespace std;

int main(){
	int x;
	cin >> x;

	for(int i = 1; i <= x; ++i){
		if(i == x){
			cout << i;
		}else{
			cout << i << " ";
		}
	}	
	cout << endl;
	return 0;
}