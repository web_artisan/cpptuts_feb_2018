#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <sstream>

using namespace std;

int main(){
	
	string numbers;
	getline(cin, numbers);

	istringstream iss(numbers);

    double a, b, c;

    iss >> a >> b >> c;

    double discriminant = pow(b, 2.0) - 4.0 * a * c ;
    double squareRoot;

    /*cout << "under square : " << discriminant << endl;
	cout << "squareRoot : " << squareRoot << endl;
	cout << "============================================" << endl;*/

    string result;

    if(discriminant > 0){
    	squareRoot = sqrt(abs(discriminant));
    	/*cout << " possitive : "<< -b - squareRoot << endl;
    	cout << " negative : " << -b + squareRoot << endl;*/

    	double firstRoot = (-b - squareRoot) / (2.0 * a);
    	double secondRoot = (-b + squareRoot) / (2.0 * a);

    	cout << firstRoot << " " << secondRoot << endl;

    }else if(discriminant == 0){
    	squareRoot = sqrt(abs(discriminant));
    	double root = -b / (2.0 * a);
    	cout << root << endl;
    }else{
    	cout << "no roots" << endl;
    }
    	
	return 0;
}