#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>

using namespace std;

int main(){
	double a,b,c;

	cin >> a >> b >> c;

	if(a < 0 && b < 0 && c < 0){
		cout << "-" << endl;
	}else if(a < 0 && b > 0 && c > 0){
		cout << "-" << endl;
	}else if(a > 0 && b > 0 && c < 0){
		cout << "-" << endl;
	}else if(a > 0 && b < 0 && c > 0){
		cout << "-" << endl;
	}else if(a > 0 && b < 0 && c < 0){
		cout << "+" << endl;
	}else if(a < 0 && b < 0 && c > 0){
		cout << "+" << endl;
	}else if(a < 0 && b > 0 && c < 0){
		cout << "+" << endl;
	}else if(a == 0 || b == 0 || c == 0){
		cout << "+" << endl;
	}else{
		cout << "+" << endl;
	}

	return 0;
}