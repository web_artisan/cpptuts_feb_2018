#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <sstream>
#include <array>
#include <vector>

using namespace std;

int main(){
	int firstArrayLength;

	string firstArrayNumbers;

	cin >> firstArrayLength;
	vector<int> firstArray;

	cin.sync();
	getline(cin, firstArrayNumbers);

	istringstream iss(firstArrayNumbers);

	for (int i = 0; i < firstArrayLength; ++i){
		int x;
		iss >> x;
		firstArray.push_back(x); 
		x = 0;
	}

	for(int i = 0; i < firstArray.size(); ++i){
		for(int x = 0; x < firstArray.size(); ++x){
			cout << firstArray[i] * firstArray[x];
			cout << " ";
		}
		
	}
	cout << endl;
	return 0;
}