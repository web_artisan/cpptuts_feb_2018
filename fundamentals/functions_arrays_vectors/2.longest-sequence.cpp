#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <vector>
#include <sstream>

using namespace std;
int main(){
	int firstArrayLength;
	string firstArrayNumbers;

	cin >> firstArrayLength;
	vector<int> firstArray;

	cin.sync();
	getline(cin, firstArrayNumbers);

	istringstream iss(firstArrayNumbers);

	for (int i = 0; i < firstArrayLength; ++i){
		int x;
		iss >> x;
		firstArray.push_back(x); 
		x = 0;
	}

    int counter = 1;
    int maxCounter = 1;
    int maxElement = firstArray[0];
    int element = firstArray[0];
 
    for (int i = 1; i < firstArray.size(); i++)
    {
      	if (firstArray[i] == element){
        	counter++;
	        if(counter > maxCounter){
	          	maxCounter = counter;
	          	maxElement = element;
	        }
      	}else{
        	counter = 1;
        	element = firstArray[i];
      	}
    }
 
    for (int j = 1; j <= maxCounter; j++){
    	cout << maxElement << " ";
	}

	return 0;
}