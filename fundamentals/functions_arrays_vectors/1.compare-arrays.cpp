#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <sstream>
#include <array>
#include <vector>

using namespace std;

bool areEqual(vector<int> firstArray, vector<int> secondArray){
	bool areEqual;

	if(firstArray.size() != secondArray.size()){
		areEqual = false;
	}else{
		int mismatchCounter = 0;
		for(int i = 0; i < firstArray.size(); ++i){
			if(firstArray[i] != secondArray[i]){
				mismatchCounter ++;
			}
		}
		if(mismatchCounter > 0){
			areEqual = false;
		}else{
			areEqual = true;
		}
	}

	return areEqual;
}

int main(){

	int firstArrayLength;
	int secondArrayLength;

	string firstArrayNumbers;
	string secondArrayNumbers;

	cin >> firstArrayLength;
	vector<int> firstArray;

	cin.sync();
	getline(cin, firstArrayNumbers);

	istringstream iss(firstArrayNumbers);

	for (int i = 0; i < firstArrayLength; ++i){
		int x;
		iss >> x;
		firstArray.push_back(x); 
		x = 0;
	}

	cin >> secondArrayLength;
	vector<int> secondArray;

	cin.sync();
	getline(cin, secondArrayNumbers);

	istringstream iss2(secondArrayNumbers);

	for (int i = 0; i < secondArrayLength; ++i){
		int x;
		iss2 >> x;
		secondArray.push_back(x);
		x = 0;
	}	

	if(areEqual(firstArray, secondArray) == true){
		cout << "equal" << endl;
	}else{
		cout << "not equal" << endl;
	}
    
	return 0;
}