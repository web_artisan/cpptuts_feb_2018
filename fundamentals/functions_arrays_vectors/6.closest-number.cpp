#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <sstream>
#include <array>
#include <vector>

using namespace std;

int main(){
	int firstArrayLength;
	int secondArrayLength;

	string firstArrayNumbers;

	cin >> firstArrayLength;
	vector<int> firstArray;

	cin.sync();
	getline(cin, firstArrayNumbers);

	istringstream iss(firstArrayNumbers);

	for (int i = 0; i < firstArrayLength; ++i){
		int x;
		iss >> x;
		firstArray.push_back(x); 
		x = 0;
	}

	vector<int> arr;
	int x = 0;

	for(int i = 0; i < firstArray.size(); ++i){
		for(int x = 0; x < firstArray.size(); ++x){
			if((firstArray[i] - firstArray[x]) == 2 || 
				(firstArray[i] - firstArray[x]) == -2 || 
				(firstArray[i] - firstArray[x]) == 4 || 
				(firstArray[i] - firstArray[x]) == -4){

				arr.push_back(firstArray[i]);
			}
		}
	}

	if(arr.size() == 2){
		cout << abs(arr[0] - arr[1]) << endl;
	}else{
		cout << 1 << endl;
	}
	
	return 0;
}