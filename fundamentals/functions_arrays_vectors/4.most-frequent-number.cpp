#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>

using namespace std;

int main(){

	int firstArrayLength;
	string firstArrayNumbers;

	vector<int> multipleMostFrequent;

	cin >> firstArrayLength;
	vector<int> firstArray;

	cin.sync();
	getline(cin, firstArrayNumbers);

	istringstream iss(firstArrayNumbers);

	for (int i = 0; i < firstArrayLength; ++i){
		int x;
		iss >> x;
		firstArray.push_back(x); 
		x = 0;
	}

	int topCount = 0, count, topElement;

	for ( int i = 0 ; i < firstArray.size(); i++){
	    count = 0;

	    for (int j = 0 ; j < firstArray.size(); j++){
	        if (firstArray[i]==firstArray[j]) count++;
	    }
	    if (count >= topCount){
	        topCount = count;
	        topElement = firstArray[i];
			if(!(find(multipleMostFrequent.begin(), multipleMostFrequent.end(), topElement) != multipleMostFrequent.end())){
				multipleMostFrequent.push_back(topElement);
			}
	     	
	    }
	}

	sort(multipleMostFrequent.begin(), multipleMostFrequent.end());

	for(int x : multipleMostFrequent){
		cout << x << " ";
	}


	return 0;
}