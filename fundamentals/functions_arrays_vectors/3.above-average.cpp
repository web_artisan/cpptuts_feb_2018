#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

int main(){

	int firstArrayLength;
	string firstArrayNumbers;

	cin >> firstArrayLength;
	vector<int> firstArray;

	cin.sync();
	getline(cin, firstArrayNumbers);

	istringstream iss(firstArrayNumbers);

	for (int i = 0; i < firstArrayLength; ++i){
		int x;
		iss >> x;
		firstArray.push_back(x); 
		x = 0;
	}

	int elementsSum = 0;
	double average;
	for(int i = 0; i < firstArray.size(); ++i){
		elementsSum += firstArray[i];
	}

	average = (elementsSum / double(firstArray.size()));

	for(int x : firstArray){
		if(x >= average){
			cout << x ;
			cout << " " ;
		}

	}
	cout << endl;

	return 0;
}