#include <iostream>
#include <iomanip>
/*#define _USE_MATH_DEFINES
#include <math.h>
#include <map>*/
#include <string>
#include <sstream>
// #include <array>
#include <vector>

using namespace std;

string replaceNonAlphaWithSpace(string s){

	for (int i = 0; i < s.size(); ++i){
		if(!isalpha(s[i]) && s[i] != ' '){
			s[i] = ' ';
		}
	}

	return s;
}

vector <string> splitBySpaceToVector(string s){
	vector <string> v;

	istringstream iss(s);

	string str;
	while(getline(iss, str, ' ')){
		string tempS = str.c_str();
		v.push_back(tempS);
	}

	return v;
}

bool compareStrings(string W1, string W2){
	int matchLettersCount = 0;
	for(int i = 0; i < W1.size(); ++i){
		for(int x = 0; x < W2.size(); ++x){
			if((W1[i] == W2[x]) && (i == x)){
				matchLettersCount++;
			}
		}
	}

	double matchingPercentage = (double(matchLettersCount) / double(W1.size())) * 100;

	if(matchingPercentage <= 40.00){
		return true;
	}else{
		return false;
	}
}

int main(){
	/*string input1, input2;

	getline(cin, input1);
	getline(cin, input2);*/

	string testS = "kittens,kidding.";
	
	cout << replaceNonAlphaWithSpace(testS) << endl;

	auto tempV = splitBySpaceToVector(replaceNonAlphaWithSpace(testS));
	cout << compareStrings(tempV[0], "kidding") << endl;

	/*for(string x : tempV){
		cout << x << endl;
	}*/


	return 0;
}