#include <iostream>
// #include <iomanip>
#include <sstream>
#include <vector>
using namespace std;

string convertIntToHexStr(int x){
	stringstream sstream;
	sstream << hex << x;
	string result = sstream.str();

	if(result == "0"){
		result += "0";
	}

	return result;
}

int main(){

	string input;
	getline(cin, input);

	vector <string> colors;
	vector <int> firstColorDecimal, secondColorDecimal;

	istringstream iss(input);

	string s;
	while(getline(iss, s, ' ')){
		string tempS = s.c_str();
		colors.push_back(tempS);
	}

	firstColorDecimal.push_back(stoul(colors[0].substr(1, 2), nullptr, 16));
	firstColorDecimal.push_back(stoul(colors[0].substr(3, 2), nullptr, 16));
	firstColorDecimal.push_back(stoul(colors[0].substr(5, 2), nullptr, 16));

	secondColorDecimal.push_back(stoul(colors[1].substr(1, 2), nullptr, 16));
	secondColorDecimal.push_back(stoul(colors[1].substr(3, 2), nullptr, 16));
	secondColorDecimal.push_back(stoul(colors[1].substr(5, 2), nullptr, 16));

	// cout << colors[1].substr(5, 2) << " : " << stoul(colors[1].substr(5, 2), nullptr, 16) << endl;

	int rawResultChunk1 = (firstColorDecimal[0] + secondColorDecimal[0]) / 2 ;
	int rawResultChunk2 = (firstColorDecimal[1] + secondColorDecimal[1]) / 2 ;
	int rawResultChunk3 = (firstColorDecimal[2] + secondColorDecimal[2]) / 2 ;

	cout 
		<< "#" 
		<< convertIntToHexStr(rawResultChunk1) 
		<< convertIntToHexStr(rawResultChunk2) 
		<< convertIntToHexStr(rawResultChunk3) 
		<< endl;

	return 0;
}