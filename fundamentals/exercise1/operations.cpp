#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <sstream>
#include <array>
#include <vector>

using namespace std;

int main(){
	
	int a, b;
	std::cin >> a >> b;

	char op;
	cin >> op;

	while(op != '+' && op != '-' && op != '*' && op != '/') {
	    cin.clear();
	    cout << "try again" << endl;
	    cin >> op;
	}

	int result;

	switch(op){
		case '+': result = a + b; break; 
		case '-': result = a - b; break;
		case '*': result = a * b; break;
		case '/': result = a / b; break;
		default: result = 0;break;
	}

	cout << result << endl;

	return 0;
}