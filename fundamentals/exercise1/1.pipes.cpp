#include <iostream>
/*#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>*/
#include <string>
#include <sstream>
#include <array>
#include <vector>

using namespace std;

int main(){
	int N;
	string checkupInput;
	string installationInput;
	
	
	vector<int> singleYearStrength;
	vector<int> lifetimes;

	cin >> N;
	vector<int> checkup;
	vector<int> installation;

	//get checkup input
	cin.ignore();

	getline(cin, checkupInput);

	istringstream iss(checkupInput);

	for(int i = 0; i < N; ++i){
		int x;
		iss >> x;
		checkup.push_back(x);
		x = 0;
	}

	//get installation input
	// cin.sync();

	getline(cin, installationInput);

	istringstream iss2(installationInput);

	for(int i = 0; i < N; ++i){
		int x;
		iss2 >> x;
		installation.push_back(x);
		x = 0;
	}

	for(int i = 0; i < installation.size(); ++i){
		int tempStrength = 0;	
		tempStrength = installation[i] - checkup[i];
		singleYearStrength.push_back(tempStrength);
	}

	for (int i = 0; i < installation.size(); ++i){
		int count = -1;
		while(installation[i] >= 0) {
			count ++;
		    installation[i] -= singleYearStrength[i];		    
		}
		lifetimes.push_back(count);
	}

	for(int x : lifetimes){
		cout << x << " ";
	}

	return 0;
}