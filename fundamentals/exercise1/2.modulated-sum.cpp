#include <iostream>
// #include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
/*#include <map>
#include <string>
#include <sstream>
#include <array>*/
#include <vector>

using namespace std;

int main(){
	int N, M, mod;

	cin >> N >> M;

	vector<vector<int>> parent;
	vector<int> child;
	vector<int> modList;
	int columntotal[2] = {0};

	for(int i = 0; i < N; ++i){
		for (int i = 0; i < M; ++i){
			int z;
			cin >> z;
			child.push_back(z);
		}
		parent.push_back(child);
		child.clear();
	}

	cin.ignore();
	cin >> mod;

	modList.resize(M);

	for(int x = 0; x < M; x++){
		int sum = 0;
		int modProduct = 0;
		for(int i = 0; i < N; i++){
			sum = sum + parent[i][x];
		}
		modProduct = sum % mod;
		modList[x] = modProduct;
	}
	

	for(int x : modList){
		cout << abs(x) << " ";
	}
	cout << endl;

	return 0;
}