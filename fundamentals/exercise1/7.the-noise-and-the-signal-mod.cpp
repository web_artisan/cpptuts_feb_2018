#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <sstream>
#include <array>
#include <vector>
#include <cctype>
#include <algorithm>
#include <typeinfo>
// #include <stdio.h>
// #include <stdlib.h>
// #include <ctype.h>

using namespace std;


bool is_number(const string& s)
{
    return !s.empty() && find_if(s.begin(), 
        s.end(), [](char c) { 
        	return !std::isdigit(c); }) == s.end();
}

int main(){

	/*string s = "123";

	cout << is_number(s) << endl;*/

	vector<string> chunks;
	vector<string> noises;
	string s;
	getline(cin, s);

	istringstream iss(s);

	string temp;
	int count;
	while(getline(iss, temp, ' ')){
		chunks.push_back(temp);
	}

	//check if all chunks are only numbers
	int numCount = 0;
	for(int i = 0; i < chunks.size(); ++i){
		if(is_number(chunks[i])){
			numCount++;
		}
	}

	if(numCount == chunks.size()){
		cout << "no noise" << endl;
	}else{
		for (int i = 0; i < chunks.size(); ++i){
			string tempNoise;
			for(int x = 0; x < chunks[i].size(); ++x){
				if(!isdigit(chunks[i][x])){
					tempNoise += chunks[i][x];
				}
			}
			noises.push_back(tempNoise);
		}
		
		sort(noises.begin(), noises.end());

		cout << noises.front() << endl;

	}

	return 0;
}