#include <iostream>
// #include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
/*#include <map>
#include <string>
#include <sstream>
#include <array>*/
#include <vector>

using namespace std;

int countDigit(int n)
{
    int count = 0;
    while (n != 0) {
        n = n / 10;
        ++count;
    }
    return count;
}

int main(){

	int N, trainDeparture;
	cin >> N;
	vector<int> busTimes;
	for(int i = 0; i < N; ++i){
		int x;
		cin >> x;
		if(countDigit(x) == 1 || countDigit(x) == 2){ // 0004
			x = 2400 + x;
		}else if(countDigit(x) == 3){
			x = 7000 + x;
		}
		busTimes.push_back(x);
	}

	cin.ignore();
	cin >> trainDeparture;
	trainDeparture = abs(trainDeparture);

	if(countDigit(trainDeparture) == 1 || countDigit(trainDeparture) == 2){ // 0004
		trainDeparture = 2400 + trainDeparture;
	}else if(countDigit(trainDeparture) == 3){
		trainDeparture = 7000 + trainDeparture;
	}

	int timeDiff = INT_MAX;
	int smallestDiffIndex = 0;
	int tempDiff = 0;
	for(int i = 0; i < busTimes.size(); ++i){
		int tempDiff = abs(busTimes[i] - trainDeparture);
		
		// cout << "tempDiff : " << tempDiff << endl;

		if(tempDiff <= timeDiff){
			timeDiff = tempDiff;
			smallestDiffIndex = i + 1;
		}
	}
	
	cout << smallestDiffIndex << endl;

	/*cout << "=====================================" << endl;
	cout << "timeDiff : " << timeDiff << endl;
	cout << "smallestDiffIndex : " << smallestDiffIndex << endl;*/

	return 0;
}