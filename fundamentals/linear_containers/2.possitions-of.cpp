#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

bool stringContainsNumbersOnly(string s){
	int nonDigits = 0;
	for(int x = 0; x < s.size(); ++x){
		if((!isdigit(s[x])) && s[x] != ' '){
			nonDigits++;
		}
	}

	if(nonDigits > 0){
		return false;
	}else{
		return true;
	}
}

vector<int> generateVector(string arrayAsString){
	vector<int> v;
	istringstream iss(arrayAsString);

	int num;
	while(iss >> num) {
	    v.push_back(num);
	}

	return v;
}

int main(){

	int rows,cols, valueToLocate;
	string rowsColsAsString;
	vector<vector<int>> container;

	getline(cin, rowsColsAsString);
	istringstream iss(rowsColsAsString);

	iss >> rows >> cols;

	for (int i = 0; i < rows; ++i){
		string rowAsString;
		getline(cin, rowAsString);

		if(stringContainsNumbersOnly(rowAsString) == 0){
			cout << "not found" << endl;
			break;
		}

		vector<int> tempVector = generateVector(rowAsString);
		container.push_back(tempVector);

	}

	cin >> valueToLocate;

	for(int x = 0; x < container.size(); x++){
		for(int y = 0; y < container[x].size(); y++){
			if(valueToLocate == container[x][y]){
				cout << x << " " << y << endl;
			}
		}
	}
	

	


	
	return 0;
}