#include <iostream>
#include <map>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

vector<int> generateVector(string arrayAsString){
	vector<int> v;
	istringstream iss(arrayAsString);

	int num;
	while(iss >> num) {
	    v.push_back(num);
	}

	return v;
}

int main(){
	
	string trainAString, trainBString;

	getline(cin, trainAString);
	vector<int> A = generateVector(trainAString);
	getline(cin, trainBString);
	vector<int> B = generateVector(trainBString);

	map<int, char> container;

	for(int i = 0; i < A.size(); ++i){
		container.insert(pair<int, char>(A[i], 'A'));
	}

	for(int i = 0; i < B.size(); ++i){
		container.insert(pair<int, char>(B[i], 'B'));
	}
	
	for(auto elem : container){
	   cout << elem.second << " ";
	}

	cout << endl;

	map<int, char>::reverse_iterator it;

    for(it = container.rbegin(); it != container.rend(); it++){
        cout << (*it).first << " ";
    }


	return 0;
}