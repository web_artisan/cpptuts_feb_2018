#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

bool compareMatrices(vector<vector<int>> m1, vector<vector<int>> m2){
	bool result;
	int errors = 0;

	if(m1.size() != m2.size()){
		errors++;
	}else{
		for(int i = 0; i < m1.size(); ++i){
			if(m1[i].size() != m2[i].size()){
				errors++;
			}else{
				for(int x = 0; x < m1[i].size(); ++x){
					if(m1[i][x] != m2[i][x]){
						errors++;
					}
				}
			}
			
		}

	}

	if(errors > 0){
		result = false;
	}else{
		result = true;
	}

	return result;
}

vector<int> generateVector(string arrayAsString){
	vector<int> v;
	istringstream iss(arrayAsString);

	int num;
	while(iss >> num) {
	    v.push_back(num);
	}

	return v;
}

int main(){

	vector<vector<int>> matrix1;
	vector<vector<int>> matrix2;

	int matrixElements1;
	cin >> matrixElements1;

	for(int i = 0; i < matrixElements1; ++i){
		cin.ignore();
		string matrixElement;
		getline(cin, matrixElement);
		
		matrix1.push_back(generateVector(matrixElement));
	}

	int matrixElements2;
	cin >> matrixElements2;

	for(int i = 0; i < matrixElements2; ++i){
		cin.ignore();
		string matrixElement;
		getline(cin, matrixElement);
		
		matrix2.push_back(generateVector(matrixElement));
	}

	if(compareMatrices(matrix1, matrix2) == true){
		cout << "equal" << endl;
	}else{
		cout << "not equal" << endl;
	}

	return 0;
}