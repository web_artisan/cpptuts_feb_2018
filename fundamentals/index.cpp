#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <sstream>
#include <array>
#include <vector>
#include <cstring>
#include <fstream>
#include <windows.h>

using namespace std;

string getCurrentDir(){
	char buf[256];
    GetCurrentDirectoryA(256, buf);
    return string(buf) + '\\';
}

int main(){

	ofstream ofs;

	ofs.open("test1.txt");

	string s {"hello world"};
	int a = 10 , b = 20;

	ofs << a << b << s;

	ofs.close();

	cout << "=====================" << endl;


	// cout << getCurrentDir() << endl;

	ifstream ifs;

	ifs.open(getCurrentDir() + "test1.txt");

	int x; string ss;
	ifs >> x >> ss;

	cout << x << "  :: " << ss << endl;
	ifs.close();

	cout << "=====================" << endl;

	ofstream output("test1.txt", fstream::app);

	string y = "\nBat Van4o";
	output << y << endl;

	output.close();


	/*string input;

	getline(cin, input);

	istringstream iss(input);

	vector<int> v;

	int num;
	while(iss >> num){
		v.push_back(num);
	}

	for(int x : v){
		cout << x << endl;
	}*/

	/*string s = "1234 , hello , 769";

	istringstream iss(s);

	int num1, num2;

	iss >> num1 >> num2;

	cout << num1 << " , " << num2 << endl;

	ostringstream oss;

	oss << "hello world " << num1 << num2 ;

	cout << oss.str() << endl;*/

	/*char str[10] {'x', 'b', 'y', '\0'};

	for (int i = 0; i < 4; ++i)
	{
		cout << str[i] << endl;
	}

	cout << "=================================" << endl;

	// strcat(str, "PHP");

	string lang = "Hypertext Preprocessor";

	string name = "Hello world " + lang;


	if(name < lang){
		cout << name << " is before " << lang << endl;
	}else{
		cout << name << " is after " << lang << endl;
	}


	// cout << name.find("Hello", ) << endl;
	string s = "Mississipi";

	int count = 0;
	for(int i = 0; i < s.size(); ++i){
		if(s[i] == 'i'){
			count++;
		}
	}

	// cout << count << endl;

	string canary = "canary";
	int foundIndex = canary.find("ry");

	while (foundIndex != string::npos) {
	  	cout << foundIndex << endl;
	  	foundIndex = canary.find("ry", foundIndex + 1);
	}

	cout << s.substr(0, 4) << endl;

	cout << s.erase(0, 4) << endl;

	cout << s.replace(0, 4, "Bat Van4o") << endl;*/



	
	return 0;
}