#include <iostream>
/*#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>*/
#include <string>
/*#include <sstream>
#include <array>
#include <vector>*/

using namespace std;

bool checkBrackets(string expression){
	int leftBracketCount = 0;
	int rightBracketCount = 0;
	for (int i = 0; i < expression.size(); ++i){
		if(expression[i] == '('){
			leftBracketCount++;
		}
		if(expression[i] == ')'){
			rightBracketCount++;
		}
	}

	if((leftBracketCount + rightBracketCount) % 2 > 0){
		return false;
	}else{
		return true;
	}
}

int main(){
	string expression;
	cin >> expression;

	if(checkBrackets(expression)){
		cout << "correct" << endl;
	}else{
		cout << "incorrect" << endl;
		
	}	

	return 0;
}