#include <iostream>
/*#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>*/
#include <string>
#include <sstream>
/*#include <array>*/
#include <vector>
/*#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>*/

using namespace std;

int main(){

	vector<string> strContainer;
	string s;
	getline(cin, s);

	istringstream iss(s);

	string temp;
	int count;
	while(getline(iss, temp, ' ')){
		int x = atoi(temp.c_str());
		if(x != 0){
			count += x;
		}else{
			if(temp != "0"){
				strContainer.push_back(temp);
			}
		}
	}

	cout << count << " ";

	for(int i = 0; i < strContainer.size(); ++i){
		cout << strContainer[i] << " " ;
	}

	return 0;
}