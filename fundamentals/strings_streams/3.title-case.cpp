#include <iostream>
/*#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>*/
#include <string>
/*#include <sstream>
#include <array>*/
#include <vector>

using namespace std;

int main(){
	
	string s;

	getline(cin, s);
	int indexToReplace = s.find(" ") + 1;

	char firstChar = s[0];
	s[0] = toupper(firstChar);

	for(int x; x < s.size(); x++){
		if(s[x] == ' ' || s[x] == ','){
			char sym = s[x + 1];
			s[x + 1] = toupper(sym);
		}
	}

	cout << s << endl;

	return 0;
}