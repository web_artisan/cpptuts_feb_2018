#include <iostream>
/*#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>*/
#include <string>
/*#include <sstream>
#include <array>
#include <vector>*/

using namespace std;

int main(){
	string s, sFind, sReplace;

	getline(cin, s);
	getline(cin, sFind);
	getline(cin, sReplace);	

	int foundIndex = s.find(sFind);
	while (foundIndex != string::npos) {
	  	s.replace(foundIndex, sFind.size(), sReplace);
	  	foundIndex = s.find(sFind, foundIndex + 1);	  	
	}

	cout << s << endl;

	return 0;
}
