#include <iostream>
/*#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>*/
#include <string>
#include <sstream>
// #include <array>
#include <vector>

using namespace std;

bool areEqual(vector<int> firstArray, vector<int> secondArray){
	bool areEqual;

	if(firstArray.size() != secondArray.size()){
		areEqual = false;
	}else{
		int mismatchCounter = 0;
		for(int i = 0; i < firstArray.size(); ++i){
			if(firstArray[i] != secondArray[i]){
				mismatchCounter ++;
			}
		}
		if(mismatchCounter > 0){
			areEqual = false;
		}else{
			areEqual = true;
		}
	}

	return areEqual;
}

vector<int> generateVector(string arrayAsString){
	vector<int> v;
	istringstream iss(arrayAsString);

	int num;
	while(iss >> num) {
	    v.push_back(num);
	}

	return v;
}

int main(){
	string arrayAsString;
	string arrayAsString2;

	getline(cin, arrayAsString);

	vector<int> firstArray = generateVector(arrayAsString);
	
	getline(cin, arrayAsString2);

	vector<int> secondArray = generateVector(arrayAsString2);	
	
	if(areEqual(firstArray, secondArray) == true){
		cout << "equal" << endl;
	}else{
		cout << "not equal" << endl;
	}
    
	return 0;
}