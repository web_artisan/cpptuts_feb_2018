#include <iostream>
#include <iomanip>
/*#define _USE_MATH_DEFINES
#include <math.h>
#include <map>*/
#include <string>
/*#include <sstream>
#include <array>*/
#include <vector>
// #include <cctype>
#include <algorithm>

using namespace std;

int main(){

	vector<string> chunks;
	vector<int> nums;
	string s;
	getline(cin, s);

	istringstream iss(s);

	string temp;
	int count;
	while(getline(iss, temp, ' ')){
		chunks.push_back(temp);
	}

	for (int i = 0; i < chunks.size(); ++i){
		string tempNum;
		for(int x = 0; x < chunks[i].size(); ++x){
			if(isdigit(chunks[i][x])){
				tempNum += chunks[i][x];
			}
		}
		int z = atoi(tempNum.c_str());
		nums.push_back(z);
	}
	sort(nums.begin(), nums.end());

	cout << nums.back() << endl;

	return 0;
}