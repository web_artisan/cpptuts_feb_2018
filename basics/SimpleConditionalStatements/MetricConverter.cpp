/*
		Да се напише програма, която преобразува разстояние между следните 8 мерни единици: m, mm, cm, mi, in, km, ft, yd. Използвайте съответствията от таблицата по-долу:
		входна единица	изходна единица
		1 meter (m)	1000 millimeters (mm)
		1 meter (m)	100 centimeters (cm)
		1 meter (m)	0.000621371192 miles (mi)
		1 meter (m)	39.3700787 inches (in)
		1 meter (m)	0.001 kilometers (km)
		1 meter (m)	3.2808399 feet (ft)
		1 meter (m)	1.0936133 yards (yd)

		Входните данни се състоят от три реда, въведени от потребителя:
		•	Първи ред: число за преобразуване
		•	Втори ред: входна мерна единица
		•	Трети ред: изходна мерна единица (за резултата)
		Резултатът да се форматира до осмия знак след десетичната запетая.
	*/
#include <iostream>
#include <string>
#include <map>
#include <iomanip>
using namespace std;

int main(){
	
	double valueToConvert;
	string inputMetric, outputMetric;

	cin >> valueToConvert >> inputMetric >> outputMetric;

	map<string, double> metrics;
	metrics["km"] = 0.001 ;
	metrics["ft"] = 3.2808399;
	metrics["mm"] = 1000;
	metrics["cm"] = 100;
	metrics["mi"] = 0.000621371192;
	metrics["in"] = 39.3700787;
	metrics["yd"] = 1.0936133;

	double outputMetricConsole = valueToConvert * (metrics[outputMetric] / metrics[inputMetric]);

	cout << fixed << setprecision(8) << outputMetricConsole << endl;
	return 0;
}

/*#include <iostream>
#include <string>
#include <iomanip>
using namespace std;
int main()
{
	double distance; 
	cin >> distance;

	string source_metric; 
	cin >> source_metric;

	string dest_metric;
	cin >> dest_metric;

	if (source_metric == "km") {
		distance = distance / 0.001;
	}
	// Check the other metrics: mm, cm, ft, yd, ...
	if (dest_metric == "ft") {
		distance = distance * 3.2808399;
	}

	if (dest_metric == "mm") {
		distance = distance * 1000;
	}

	if (dest_metric == "cm") {
		distance = distance * 100;
	}

	if (dest_metric == "mi") {
		distance = distance * 0.000621371192;
	}

	if (dest_metric == "in") {
		distance = distance * 39.3700787;
	}

	if (dest_metric == "yd") {
		distance = distance * 1.0936133;
	}

	// Check the other metrics: mm, cm, ft, yd, ...
	cout << fixed << setprecision(8) << distance << endl;

	return 0;
}*/
