#include <iostream>

using namespace std;

int main(){
	/*
		Дадено е цяло число – брой точки. Върху него се начисляват бонус точки по правилата, описани по-долу. 
		Да се напише програма, която пресмята бонус точките за това число и общия брой точки с бонусите.
		•	Ако числото е до 100 включително, бонус точките са 5.
		•	Ако числото е по-голямо от 100, бонус точките са 20% от числото.
		•	Ако числото е по-голямо от 1000, бонус точките са 10% от числото.
		•	Допълнителни бонус точки (начисляват се отделно от предходните):
		o	За четно число -> + 1 т.
		o	За число, което завършва на 5 -> + 2 т.
	*/

	int number;
	double sum;
	double bonusScore = 0.0;
	double bonusPercentage;

	cin >> number;

	if(number <= 100){
		bonusScore += 5;
	}else if(number > 100 && number <= 1000){
		bonusPercentage = number * 0.20;		
	}else if(number > 1000){
		bonusPercentage += number * 0.10;
	}	

	bonusScore += bonusPercentage;

	//extra bonus
	if(number % 10 == 5){
		bonusScore += 2;
	}else if(number % 2 == 0){
		bonusScore += 1;
	}

	sum = number + bonusScore;

	cout << bonusScore << endl;
	cout << sum << endl;

	return 0;
}