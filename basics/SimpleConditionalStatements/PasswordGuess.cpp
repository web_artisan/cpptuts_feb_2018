/*
	Да се напише програма, която чете парола (един ред с произволен текст), въведена от потребителя, и проверява дали въведеното съвпада с фразата “s3cr3t!P@ssw0rd”. 
	При съвпадение да се изведе “Welcome”. При несъвпадение да се изведе “Wrong password!”.
*/
#include <iostream>
#include <string>

using namespace std;

int main(){
	string passInput;
	getline(cin, passInput);
	string pass = "s3cr3t!P@ssw0rd";

	if(passInput == pass){
		cout << "Welcome" << endl;
	}else{
		cout << "Wrong password!" << endl;
	}
	
	return 0;
}