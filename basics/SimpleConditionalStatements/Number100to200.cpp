/*
	Да се напише програма, която чете цяло число, въведено от потребителя, и проверява дали е под 100, между 100 и 200 или над 200. 
	Да се отпечатат съответно съобщения като в примерите по-долу.

	вход	изход			вход	изход				вход	изход
	95	Less than 100		120	Between 100 and 200		210	Greater than 200
	Тествайте решението си в judge системата: https://judge.softuni.bg/Contests/Practice/Index/530#9.
	Подсказка: използвайте if-else-if-else конструкция за да проверите всеки от трите случая.

*/

#include <iostream>

using namespace std;

int main(){
	int num;
	cin >> num;

	if(num < 100){
		cout << "Less than 100" << endl;
	}
	else if(num >= 100 && num <= 200){
		cout << "Between 100 and 200" << endl;
	}
	else if(num > 200){
		cout << "Greater than 200" << endl;
	}

	return 0;
}