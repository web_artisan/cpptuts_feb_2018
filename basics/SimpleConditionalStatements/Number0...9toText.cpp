#include <iostream>
#include <map>
#include <string>
using namespace std;

int main(){

	int userInputNumber;
	cin >> userInputNumber;
	
	map<int, string> numbers;
	numbers[0] = "zero";
	numbers[1] = "one";
	numbers[2] = "two";
	numbers[3] = "three";
	numbers[4] = "four";
	numbers[5] = "five";
	numbers[6] = "six";
	numbers[7] = "seven";
	numbers[8] = "eight";
	numbers[9] = "nine";
	// numbers[10] = "ten";

	if(numbers.count(userInputNumber) > 0){
    	cout << numbers[userInputNumber] << endl;
    }else{
    	cout << "number too big" << endl;
    }

	return 0;
}