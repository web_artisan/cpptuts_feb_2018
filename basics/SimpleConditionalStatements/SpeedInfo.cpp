/*
	Да се напише програма, която чете скорост (десетично число), въведена от потребителя, и отпечатва информация за скоростта. При скорост до 10 (включително) отпечатайте “slow”. 
	При скорост над 10 и до 50 отпечатайте “average”. 
	При скорост над 50 и до 150 отпечатайте “fast”. При скорост над 150 и до 1000 отпечатайте “ultra fast”. При по-висока скорост от 1000 отпечатайте “extremely fast”.

	8	slow		49.5	average		126	fast		160	ultra fast		3500	extremely fast
*/
#include <iostream>
#include <cmath>
using namespace std;

int main(){
	double speed;
	cin >> speed;

	double ceilSpeed = ceil(speed);

	// cout << ceilSpeed << endl;

	if(ceilSpeed <= 10){
		cout << "slow" << endl;
	}else if(ceilSpeed > 10 && ceilSpeed <= 50 ){
		cout << "average" << endl;
	}else if(ceilSpeed > 50 && ceilSpeed <= 150){
		cout << "fast" << endl;
	}else if(ceilSpeed > 150 && ceilSpeed <= 1000){
		cout << "ultra fast" << endl;
	}else if(ceilSpeed > 1000){
		cout << "extremely fast" << endl;
	}

	return 0;
}