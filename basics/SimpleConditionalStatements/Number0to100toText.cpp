/*
	Да се напише програма, която превръща число [0…100] в текст: 25 -> “twenty five”
*/
#include <iostream>
#include <map>
#include <string>

using namespace std;

int main(){

	int num;
	cin >> num;

	int result = num / 10;
	int remainder = num % 10;

	map<int, string> tens;
	tens[1] = "eleven";
	tens[2] = "twelve";
	tens[3] = "thirteen";
	tens[4] = "fourteen";
	tens[5] = "fifteen";
	tens[6] = "sixteen";
	tens[7] = "seventeen";
	tens[8] = "eighteen";
	tens[9] = "nineteen";

	map<int, string> rounded;
	rounded[1] = "ten";
	rounded[2] = "twenty";
	rounded[3] = "thirty";
	rounded[4] = "forty";
	rounded[5] = "fifty";
	rounded[6] = "sixty";
	rounded[7] = "seventy";
	rounded[8] = "eighty";
	rounded[9] = "ninety";
	rounded[10] = "one hundred";

	map<int, string> units;
	units[1] = "one";
	units[2] = "two";
	units[3] = "three";
	units[4] = "four";
	units[5] = "five";
	units[6] = "six";
	units[7] = "seven";
	units[8] = "eight";
	units[9] = "nine";

	/*cout << "result : " << result << endl;
	cout << "remainder : " << remainder << endl;*/

	/*for (int i = 0; i <= 100; ++i){
		int result = i / 10;
		int remainder = i % 10;*/
	if(num < 0 || num > 100) {
		cout << "invalid number" << endl;
	}else{
		if(result == 0 && remainder == 0){
			cout << "zero" << endl;
		}else if(result > 0 && remainder > 0){
			if(result == 1 && remainder > 0){
				cout << tens[remainder] << endl;
			}else if(result > 1 && remainder > 0){
				cout << rounded[result] << " " << units[remainder] << endl;
			}
		}else if(result > 0 && remainder == 0){
			cout << rounded[result] << endl;
		}else if(result == 0 && remainder > 0){
			cout << units[remainder] << endl;
		}
	}
		
	// }
	


	return 0;
	
}