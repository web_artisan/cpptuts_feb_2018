#include <iostream>

using namespace std;

int main()
{
	int sec1, sec2, sec3;

	cin >> sec1 >> sec2 >> sec3;
	// TODO: Read also sec1, sec2 and sec3 …
	int secs = sec1 + sec2 + sec3;

	int minutes = secs / 60;
	int left_seconds = secs % 60;
	

	if (left_seconds < 10) {
		cout << minutes << ":0" << left_seconds << endl;
	}
	else {
		cout << minutes << ":" << left_seconds << endl;
	}

	return 0;
}
