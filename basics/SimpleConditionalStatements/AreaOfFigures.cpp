/*
	Да се напише програма, в която потребителят въвежда вида и размерите на геометрична фигура и пресмята лицето й. 
	Фигурите са четири вида: 
	квадрат (square), 
	правоъгълник (rectangle), 
	кръг (circle) и 
	триъгълник (triangle). 

	На първия ред на входа се чете вида на фигурата (square, rectangle, circle или triangle). 
	Ако фигурата е квадрат, на следващия ред се чете едно число – дължина на страната му. 
	Ако фигурата е правоъгълник, на следващите два реда четат две числа – дължините на страните му. 
	Ако фигурата е кръг, на следващия ред чете едно число – радиусът на кръга. 
	Ако фигурата е триъгълник, на следващите два реда четат две числа – дължината на страната му и дължината на височината към нея. 
	Резултатът да се закръгли до 3 цифри след десетичната точка.

	Тествайте решението си в judge системата: https://judge.softuni.bg/Contests/Practice/Index/530#12 .
	Подсказка: използвайте серия от if-else-if-else-… конструкции, за да обработите 4-те вида фигури.

*/
#include <iostream>
#include <string>
#define _USE_MATH_DEFINES
#include <math.h>
#include <iomanip>

using namespace std;

int main(){
	
	string figureType;
	cin >> figureType;

	double face;

	if(figureType == "square"){
		double a;
		cin >> a;
		face = a * a;
	}else if(figureType == "rectangle"){
		double a, b;
		cin >> a >> b;
		face = a * b;
	}else if(figureType == "circle"){
		double r;
		cin >> r;
		face = M_PI * pow(r, 2);
	}else if(figureType == "triangle"){
		double a, h;
		cin >> a >> h;
		face = a * h / 2;
		// S = ½(a.ha);
	}

	cout << fixed << setprecision(3) << face << endl;

	return 0;
}