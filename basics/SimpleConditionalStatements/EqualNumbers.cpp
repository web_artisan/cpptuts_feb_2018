/*
	Три еднакви числа: да се въведат 3 числа и да се отпечата дали са еднакви (yes / no).
*/

#include <iostream>

using namespace std;

int main(){

	int a,b,c;
	cin >> a >> b >> c;

	if(a == b && b == c && c == a){
		cout << "yes" << endl;
	}else{
		cout << "no" << endl;
	}

	return 0;
}