#include <iostream>

using namespace std;

void validateRectangleSide(int side){
	while(cin.fail()) {
        cout << "Error : Side must a number" << endl;
        cin.clear();
        cin.ignore(256,'\n');
        cin >> side;
    }
};

int main(){
	int a;
	int b;
	int face;

	cout << "Enter side A :" << endl;
	cin >> a;
	while(cin.fail()) {
        cout << "Error : Side A must a number" << endl;
        cin.clear();
        cin.ignore(256,'\n');
        cin >> a;
    }
	// validateRectangleSide(a);

	cout << "Enter side B :" << endl;
	cin >> b;
	// validateRectangleSide(b);
	while(cin.fail()) {
        cout << "Error : Side B must a number" << endl;
        cin.clear();
        cin.ignore(256,'\n');
        cin >> b;
    }

	face = a * b;

	// cout << "The rectangle face is : " << face << " cm2" << endl;
	cout << face << endl;
	return 0;
}