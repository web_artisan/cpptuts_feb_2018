#include <iostream>

using namespace std;

int main(){
	int starsCount;

	// cout << "Enter stars count : " << endl;
	cin >> starsCount;
	/*while(cin.fail()){
		cout << "Error : Count must a number" << endl;
        cin.clear();
        cin.ignore(256,'\n');
        cin >> starsCount;
	}*/

	for(int x = 1; x <= starsCount; ++x){
		if(x == 1 || x == starsCount){
			for(int i = 1; i <= starsCount; ++i){
				cout << "*";
			}
		}else{
			cout << "*";
			for(int y = 1; y <= starsCount - 2; ++y){
				cout << " ";
			}
			cout << "*";
		}
		cout << endl;
	}

	return 0;
}