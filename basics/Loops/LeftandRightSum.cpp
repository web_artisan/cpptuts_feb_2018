/*
	Да се напише програма, която чете 2*n-на брой цели числа, подадени от потребителя, 
	и проверява дали сумата на първите n числа (лява сума) е равна на сумата на вторите n числа (дясна сума). 
	При равенство печата "Yes" + сумата; иначе печата "No" + разликата. Разликата се изчислява като положително число (по абсолютна стойност). Примери:
*/

#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

int main(){
	
	int numbersCount;
	cin >> numbersCount;

	int sum1 = 0; 
	int sum2 = 0;

	for(int i = 0; i < numbersCount; ++i){
		int x;
		cin >> x;
		sum1 += x;
	}

	for(int i = 0; i < numbersCount; ++i){
		int x;
		cin >> x;
		sum2 += x;
	}

	if(sum1 == sum2){
		cout << "Yes, sum = " << sum1 << endl;
	}else{
		int absolute = abs(sum1 - sum2);
		cout << "No, diff = " << absolute << endl;
	}

	return 0;
}