#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

int main(){

    int n;
    cin >> n;
    int currSum = 0;
    int prevSum = 0;
    int diff = 0;
    int maxDiff = 0;
    
    for (int i = 0; i < n; i++){
        prevSum = currSum;
        currSum = 0;
        int x, y;
        cin >> x >> y;
        currSum += x;
        currSum += y;
        if (i != 0)
        {
            diff = abs(currSum - prevSum);

            if (diff != 0 && diff > maxDiff)
            {
                maxDiff = diff;
            }
        }
        
    }


    if (prevSum == currSum || n == 1){
        cout << "Yes, value=" << currSum << endl;
    }
    else{
        cout << "No, maxdiff=" << maxDiff << endl;
    }
}