#include <iostream>
using namespace std;
int main() {
	int n, number; 
	cin >> n;
	int min = INT_MAX; // smallest integer value

	for (int i = 1; i <= n; i++) {
	  	cin >> number;
	  	if(number < min){
	  		min = number;
	  	}
	}

	cout << min << endl;
}