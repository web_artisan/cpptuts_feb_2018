#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main(){

    string word1;
    string word2;

    cin >> word1 >> word2;

    transform(word1.begin(), word1.end(), word1.begin(), ::tolower);
    transform(word2.begin(), word2.end(), word2.begin(), ::tolower);

    if(word1 == word2){
        cout << "yes" << endl;
    }else if (word1 != word2){
        cout << "no" << endl;
    }
}