#include <iostream>
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>
#include <float.h>

using namespace std;

int main(){

    double n;
    cin >> n;
    double oddSum = 0.0;
    double evenSum = 0.0;
    double oddMax = DBL_MIN;
    double evenMax = DBL_MIN;
    double oddMin = DBL_MAX;
    double evenMin = DBL_MAX;

    for (int i = 1; i <= n; i++)
    {
        double num;
        cin >> num;

        if (i % 2 == 0)
        {
            if (num > evenMax)
            {
                evenMax = num;
            }
            if (num < evenMin)
            {
                evenMin = num;
            }
            evenSum += num;

        }
        else
        {

            if (num > oddMax)
            {
                oddMax = num;
            }
            if (num < oddMin)
            {
                oddMin = num;
            }

            oddSum += num;
        }
    }

    // cout << "OddSum=" << oddSum << "," << endl;            
    cout << "OddSum=" << oddSum << ", ";
    
    if (oddMin == DBL_MAX)
    {
        // cout << "OddMin=No," << endl;
        cout << "OddMin=No, ";
    }
    else
    {   
        // cout << "OddMin=" << oddMin << "," << endl;
        cout << "OddMin=" << oddMin << ", ";
    }
    if (oddMax == DBL_MIN)
    {
        // cout << "OddMax=No," << endl;
        cout << "OddMax=No, ";
    }
    else
    {
        // cout << "OddMax=" << oddMax << "," << endl;
        cout << "OddMax=" << oddMax << ", ";
    }

    // cout << "EvenSum=" << evenSum << "," << endl;
    cout << "EvenSum=" << evenSum << ", ";

    if (evenMin == DBL_MAX)
    {
        // cout << "EvenMin=No," << endl;
        cout << "EvenMin=No, ";
    }
    else
    {
        // cout << "EvenMin=" << evenMin << "," << endl;
        cout << "EvenMin=" << evenMin << ", ";
    }
    if (evenMax == DBL_MIN)
    {
        // cout << "EvenMax=No" << endl;
        cout << "EvenMax=No" << endl;
    }
    else
    {
        // cout << "EvenMax=" << evenMax << endl;
        cout << "EvenMax=" << evenMax << endl;
    }
}