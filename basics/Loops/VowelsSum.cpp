/*
	Да се напише програма, която чете текст (стринг), въведен от потребителя, 
	и изчислява и отпечатва сумата от стойностите на гласните букви според таблицата по-долу:
	буква		a	e	i	o	u
	стойност	1	2	3	4	5

*/

#include <iostream>
#include <string>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <typeinfo>
using namespace std;

int main(){

	string userStr;
	int vowelsSum = 0;
	map<char, int> vowels;
	vowels['a'] = 1;
	vowels['e'] = 2;
	vowels['i'] = 3;
	vowels['o'] = 4;
	vowels['u'] = 5;

	getline(cin, userStr);

	for (int i = 0; i < userStr.length(); ++i){
		if(vowels.count(userStr[i]) != 0){
			vowelsSum += vowels[userStr[i]];
			// cout << "Vowel : " << userStr[i] << " " << vowels[userStr[i]] << endl;

		}/*else{
			cout << "Consonant : " << userStr[i] << " " << vowels[userStr[i]] << endl;
		}*/
	}
	cout << vowelsSum << endl;
	return 0;
}