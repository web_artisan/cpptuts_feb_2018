/*
	Да се напише програма, която чете n-на брой цели числа, подадени от потребителя, 
	и проверява дали сумата от числата на четни позиции е равна на сумата на числата на нечетни позиции. При равенство да се 
	отпечата "Yes" + сумата; иначе да се отпечата "No" + разликата. Разликата се изчислява по абсолютна стойност. Примери:
*/

/*#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

int main(){
	
	int numbersCount;
	cin >> numbersCount;

	int sum1 = 0; 
	int sum2 = 0;

	for(int i = 0; i < numbersCount; ++i){
		int x;
		cin >> x;
		if(i % 2 == 0){
			sum1 += x;
		}else{
			sum2 += x;
		}
	}

	if(sum1 == sum2){
		cout << "Yes Sum = " << sum1 << endl;
	}else{
		int absolute = abs(sum1 - sum2);
		cout << "No Diff = " << absolute << endl;
	}

	return 0;
}*/

#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

int main(){
	int number; 
	cin >> number;
	int odd_sum = 0;
	int even_sum = 0;

	for(int i = 0; i < number; i++){
	  	int current_num; 
	  	cin >> current_num;
	  	if (i % 2 == 0)
	    	even_sum += current_num;
	  	else
	    	odd_sum += current_num;
	}

	if(odd_sum == even_sum){
		cout << "Yes" << endl;
		cout << "Sum = " << odd_sum << endl;
	}else{
		int absolute = abs(odd_sum - even_sum);
		cout << "No" << endl;
		cout << "Diff = " << absolute << endl;
	}
}
