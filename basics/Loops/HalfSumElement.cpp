/*
	Да се напише програма, която чете n-на брой цели числа, въведени от потребителя, 
	и проверява дали сред тях съществува число, което е равно на сумата на всички останали. 
	Ако има такъв елемент, печата "Yes", "Sum = "  + неговата стойност; 
	иначе печата "No", "Diff = " + разликата между най-големия елемент и сумата на останалите (по абсолютна стойност).
*/

#include <iostream>
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>
/*#include <array>
#include <vector>
#include <iterator>*/

using namespace std;

int main(){
	int numbersCount;
	cin >> numbersCount;

	// int numbers[numbersCount];
	int sumNums = 0;
	int y = 0;
	for(int i = 0; i < numbersCount; i++){
		int x;
		cin >> x;
		sumNums += x;
		if(x > y){
			y = x;
		}
	}

	/*cout << "SUM :" << sumNums << endl;
	cout << "MAx NUM :" << y << endl;*/

	if((sumNums - y) == y){
		cout << "Yes " << endl;
		cout << "Sum = " << (sumNums - y) << endl;
	}else{
		int absolute = abs((sumNums - y) - y);
		cout << "No " << endl;
		cout << "Diff = " << absolute << endl;
	}

	/*cout << "SUM :" << sumNums << endl;
	cout << "MAx NUM :" << y << endl;*/

	/*cout << "============================" << endl;

	for (int i = 0; i < sizeof(numbers)/sizeof(numbers[0]); i++){
		cout << numbers[i] << endl;
	}*/

	/*cout << "Array first element : " << numbers[0] << endl;
	cout << "Array last element : " << sizeof(numbers) - 1 << endl;*/

	/*cout << "Array first element : " << numbers.front() << endl;
	cout << "Array last element : " << numbers.back() << endl;*/

	// max_element(begin(numbers), end(numbers));
	// max_element(numbers.begin(), numbers.end());

	return 0;
}