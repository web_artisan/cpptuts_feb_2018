#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int main(){
	/*
		Напишете програма, която чете от конзолата страна и височина на триъгълник, въведени от потребителя, и пресмята неговото лице. 
		Използвайте формулата за лице на триъгълник: area = a * h / 2. Закръглете резултата до 2 знака след десетичната точка използвайки fixed и setprecision.
	*/

	double a, h;
	cin >> a >> h;

	double area = abs((a * h) / 2);

	cout << fixed << setprecision(2) << area << endl;
	return 0;
}