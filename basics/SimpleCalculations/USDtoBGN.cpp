#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	/*
		Напишете програма за конвертиране на щатски долари (USD) в български лева (BGN). 
		Закръглете резултата до 2 цифри след десетичната точка. 
		Използвайте фиксиран курс между долар и лев: 1 USD = 1.79549 BGN.
	*/

	double dollars;
	double fixing = 1.79549;
	cin >> dollars;

	double levas = dollars * fixing;
	cout << fixed << setprecision(2) << levas << endl;

	return 0;
}