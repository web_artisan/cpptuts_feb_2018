#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include <iomanip>
using namespace std;

int main(){
	/*
		Напишете програма, която чете ъгъл в радиани (rad), въведен от потребителя, и го преобразува в градуси (deg).
		Потърсете в Интернет подходяща формула. Декларирайте π  до 4 знак.
		degrees = radians * (180 / pi);
	*/
	double radians;
	cin >> radians;

	double degrees = floor(radians * (180 / M_PI));

	cout << degrees << endl;

	return 0;
}