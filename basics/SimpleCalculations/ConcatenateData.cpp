#include <iostream>
#include <string>

using namespace std;

int main(){
	string first_name, last_name, town;
	int age;

	getline(cin, first_name);
	getline(cin, last_name);
	cin >> age;
	cin.ignore(256, '\n');
	// getline(cin, age);
	getline(cin, town);

	cout << "You are " << first_name << " " << last_name << ", a " << age << "-years old person from " << town << "." << endl;
	return 0;
}