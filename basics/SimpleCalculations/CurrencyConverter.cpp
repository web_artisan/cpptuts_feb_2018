#include <iostream>
#include <map>
#include <iomanip>

using namespace std;

int main(){
	/*
		Напишете програма за конвертиране на парична сума от една валута в друга. 
		Трябва да се поддържат следните валути: BGN, USD, EUR, GBP. Използвайте следните фиксирани валутни курсове:
		Курс	USD			EUR			GBP
		1 BGN	1.79549		1.95583		2.53405

	*/

	map<string, double> currencies;
	currencies["BGN"] = 1;
    currencies["USD"] = 1.79549;
    currencies["EUR"] = 1.95583;
    currencies["GBP"] = 2.53405;

    string inputCurrency1;
	string inputCurrency2;
	double amount;

	cin >> amount >> inputCurrency1 >> inputCurrency2;

	double result = (amount / currencies[inputCurrency2]) * currencies[inputCurrency1];

    cout << fixed << setprecision(2) << result << " " << inputCurrency2 << endl;

	return 0;
}