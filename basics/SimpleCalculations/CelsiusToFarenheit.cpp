#include <iostream>
#include <iomanip>
#include <tgmath.h>
// #include <cfenv>
using namespace std;

int main(){
	double celsius;
	cin >> celsius;

	double farenheit = (celsius * 9/5) + 32;

	cout << left << setfill(' ') << setw(4) << farenheit << endl;

	// cout << "Fmod : " << fmod(farenheit, 1.0) << endl;
	// cout << printf("%d.2", farenheit) << endl;

	/*if(fmod(farenheit, 1.0) != 0){
		cout << fixed << setprecision(2) << farenheit << endl;
	}else{
		cout << round(farenheit) << endl;
	}*/

	return 0;
}