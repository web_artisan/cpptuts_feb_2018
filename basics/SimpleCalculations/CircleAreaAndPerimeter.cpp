#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include <iomanip>
using namespace std;

int main(){
	
	double radius;
	cin >> radius;

	double circleArea = M_PI * pow(radius, 2);
	double circlePerimeter = 2 * M_PI * radius;

	cout << fixed << setprecision(4) << circleArea << endl;
	cout << fixed << setprecision(4) << circlePerimeter << endl;

	return 0;
}