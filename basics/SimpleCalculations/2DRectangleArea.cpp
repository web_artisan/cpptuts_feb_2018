#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main(){
	/*
		Правоъгълник е зададен с координатите на два от своите срещуположни ъгъла (x1, y1) – (x2, y2). 
		Да се пресметнат площта и периметъра му. Входът се въвежда от потребителя. 
		Числата x1, y1, x2 и y2 са дадени по едно наред. 
		Изходът се извежда на конзолата и трябва да съдържа два реда с по една число на всеки от тях – лицето и периметъра.
	*/

	double x1, y1, x2, y2;
  	cin >> x1 >> y1 >> x2 >> y2;
  	double area = abs(x2 - x1) * abs(y1 - y2);
  	double perimeter = 2 * (abs(x2 - x1) + abs(y1 - y2));
  	cout << area << endl; 
  	cout << perimeter << endl;


	/*double x1, x2, y1, y2;
	cin >> x1 >> y1 >> x2 >> y2;

	double sideA = abs(x1 - x2);
	double sideB = abs(y1 - y2);

	double rectangleArea = sideA * sideB;
	double rectanglePerimeter = 2 * (sideA + sideB);*/

	/*cout << setprecision(4) << rectangleArea << endl;
	cout << setprecision(4) << rectanglePerimeter << endl;*/

	/*if(fmod(rectangleArea, 1.0) != 0){
		cout << fixed << setprecision(4) << rectangleArea << endl;
	}else{
		cout << round(rectangleArea) << endl;
	}

	if(fmod(rectanglePerimeter, 1.0) != 0){
		cout << fixed << setprecision(4) << rectanglePerimeter << endl;
	}else{
		cout << round(rectanglePerimeter) << endl;
	}*/

	/*cout << left << setfill(' ') << setw(4) << rectangleArea << endl;
	cout << left << setfill(' ') << setw(4) << rectanglePerimeter << endl;*/

	/*cout << fixed << setprecision(2) << rectangleArea << endl;
	cout << fixed << setprecision(2) << rectanglePerimeter << endl;*/

	return 0;
}