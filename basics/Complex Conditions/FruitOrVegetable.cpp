/*
	Да се напише програма, която чете име на продукт, въведено от потребителя, и проверява дали е плод или зеленчук.
•	Плодовете "fruit" са banana, apple, kiwi, cherry, lemon и grapes
•	Зеленчуците "vegetable" са tomato, cucumber, pepper и carrot
•	Всички останали са "unknown"
	Да се изведе "fruit", "vegetable" или "unknown" според въведения продукт.
*/

#include <iostream>
#include <string>

using namespace std;

int main(){

	string userInput;

	cin >> userInput;

	if(userInput == "banana" || userInput == "apple" || userInput == "kiwi" || userInput == "cherry" || userInput == "lemon" || userInput == "grapes"){
		cout << "fruit" << endl;
	}else if(userInput == "tomato" || userInput == "cucumber" || userInput == "pepper" || userInput == "carrot"){
		cout << "vegetable" << endl;
	}else{
		cout << "unknown" << endl;
	}

	return 0;
}