/*
	48 уикенда в годината, разделени по следния начин:
	•	46 уикенда в София  46 * 3 / 4  34.5 съботни игри в София
	•	2 уикенда в родния си град  2 недели  2 игри в неделя в родния град
	5 празника:
	•	5 * 2/3  3.333 игри в София в празничен ден
	Общо игри през уикенди и празници в София и в родния град: 34.5 + 2 + 3.333  39.833
	Годината е високосна:
	•	Влади играе допълнителни 15% * 39.833  5.975 игри волейбол
	Общо игри през цялата година:
	•	39.833 + 5.975 = 45.808 игри
	•	Резултатът е 45 (закръгля се надолу)

	•	Първият ред съдържа думата "leap" (високосна година) или "normal" (невисокосна).
	•	Вторият ред съдържа цялото число p – брой празници в годината (които не са събота и неделя).
	•	Третият ред съдържа цялото число h – брой уикенди, в които Влади си пътува до родния град.


	•	Пресметнете уикендите в София (48 минус уикендите в родния град). 
		Пресметнете броя игри в уикендите в София: умножете уикендите в София с (3.0 / 4). 
		Обърнете внимание, че трябва да се използва дробно деление (3.0 / 4), а не целочислено (3 / 4).

	•	Пресметнете броя игри в родния град. Те са точно колкото са пътуванията до родния град.
	•	Пресметнете броя игри в празничен ден. Те са броя празници умножени по (2.0 / 3).
	•	Сумирайте броя на всички игри. Той е дробно число. Не бързайте да закръгляте още.
	•	Ако годината е високосна, добавете 15% към общия брой игри.
	•	Накрая закръглете надолу до най-близкото цяло число с floor(result).



*/

#include <iostream>
#include <iomanip>
#include <string>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

int main(){
	string year;
	int p,h;

	double games;

	cin >> year >> p >> h;

	if(year == "leap"){
		double weekendSofiaGames = (48 - h) * 3.0 / 4;
		double weekendHomeGames = h;
		double holidayGames = p * 2.0/3;
		double normalGames = weekendSofiaGames + weekendHomeGames + holidayGames;
		double leapYearGameBonus = 0.15 * normalGames;

		games = normalGames + leapYearGameBonus;
	}else if(year == "normal"){
		
		double weekendSofiaGames = (48 - h) * 3.0 / 4;
		double weekendHomeGames = h;
		double holidayGames = p * 2.0/3;
		double normalGames = weekendSofiaGames + weekendHomeGames + holidayGames;
		games = normalGames;
	}

	cout << floor(games) << endl;

	return 0;
}