/*
	Напишете програма, която чете от конзолата плод (banana / apple / orange / grapefruit / kiwi / pineapple / grapes), 
	ден от седмицата (Monday / Tuesday / Wednesday / Thursday / Friday / Saturday / Sunday) и количество (десетично число) , 
	въведени от потребителя, и пресмята цената според цените от таблиците по-горе. Резултатът да се отпечата закръглен с 2 цифри след десетичната точка. 
	При невалиден ден от седмицата или невалидно име на плод да се отпечата "error ".
*/
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;
int main() {

	string day, fruit;
	double quantity;

	cin >> fruit >> day >> quantity;

	if (day == "Saturday" || day == "Sunday") {
	  	if (fruit == "banana"){
	  		cout << fixed << setprecision(2) << quantity * 2.7 << endl;
	  	}else if (fruit == "apple"){
			cout << fixed << setprecision(2) << quantity * 1.25 << endl;
	  	}else if (fruit == "orange"){
	  		cout << fixed << setprecision(2) << quantity * 0.9 << endl;
	  	}else if (fruit == "grapefruit"){
	  		cout << fixed << setprecision(2) << quantity * 1.6 << endl;
	  	}else if (fruit == "kiwi"){
	  		cout << fixed << setprecision(2) << quantity * 3 << endl;
	  	}else if (fruit == "pineapple"){
	  		cout << fixed << setprecision(2) << quantity * 5.6 << endl;
	  	}else if (fruit == "grapes"){
	  		cout << fixed << setprecision(2) << quantity * 4.2 << endl;
	  	}else{
	  		cout << "error" << endl;	
	  	}
	}else if (day == "Monday" || day == "Tuesday" || day == "Wednesday" 
		|| day == "Thursday" || day == "Friday"
		){
		if (fruit == "banana"){
	  		cout << fixed << setprecision(2) << quantity * 2.5 << endl;
	  	}else if (fruit == "apple"){
			cout << fixed << setprecision(2) << quantity * 1.2 << endl;
	  	}else if (fruit == "orange"){
	  		cout << fixed << setprecision(2) << quantity * 0.85 << endl;
	  	}else if (fruit == "grapefruit"){
	  		cout << fixed << setprecision(2) << quantity * 1.45 << endl;
	  	}else if (fruit == "kiwi"){
	  		cout << fixed << setprecision(2) << quantity * 2.7 << endl;
	  	}else if (fruit == "pineapple"){
	  		cout << fixed << setprecision(2) << quantity * 5.5 << endl;
	  	}else if (fruit == "grapes"){
	  		cout << fixed << setprecision(2) << quantity * 3.85 << endl;
	  	}else{
	  		cout << "error" << endl;	
	  	}
  	}else {
  		cout << "error" << endl;
  	}

}
