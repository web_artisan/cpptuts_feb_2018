/*
	Напишете програма, която:
	Чете от потребителя: град, обем на продажби (десетично число)
	Изчислява комисионната, която дадена фирма дава на търговците според града и обема на продажбите
	Извежда стойността на комисионната, закръглена до 2 цифри след десетичната запетая
*/

#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main() {
	string town; double sales;
	cin >> town >> sales;
	double comission = -1;

	if (town == "Sofia") {
	  	if (sales >= 0 && sales <= 500){
	  		comission = 0.05;
	  	}else if (sales > 500 && sales <= 1000){
	  		comission = 0.07;
	  	}else if(sales > 1000 && sales <= 10000){
	  		comission = 0.08;
	  	}else if(sales > 10000){
	  		comission = 0.12;
	  	}
	}else if (town == "Varna"){
		if (sales >= 0 && sales <= 500){
	  		comission = 0.045;
	  	}else if (sales > 500 && sales <= 1000){
	  		comission = 0.075;
	  	}else if(sales > 1000 && sales <= 10000){
	  		comission = 0.10;
	  	}else if(sales > 10000){
	  		comission = 0.13;
	  	}
	}else if (town == "Plovdiv"){
		if (sales >= 0 && sales <= 500){
	  		comission = 0.055;
	  	}else if (sales > 500 && sales <= 1000){
	  		comission = 0.08;
	  	}else if(sales > 1000 && sales <= 10000){
	  		comission = 0.12;
	  	}else if(sales > 10000){
	  		comission = 0.145;
	  	}
			
	}

	if (comission >= 0){
		cout << fixed << setprecision(2) << comission * sales << endl;
	}else{ 
		cout << "error" << endl;
	}
	
}
