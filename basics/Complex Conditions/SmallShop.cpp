/*
	Напишете програма, която:
	Чете от потребителя име на продукт, град, количество:
	Пресмята цената на продукта спрямо таблицата:
*/

#include <iostream>
#include <string>
using namespace std;

int main(){
	string product, town;
	double quantity;
	cin >> product >> town >> quantity;

	if (town == "Sofia") {
		if (product == "coffee"){
	    	cout << quantity * 0.5 << endl;
	  	}else if(product == "water"){
	  		cout << quantity * 0.8 << endl;
	  	}else if(product == "beer"){
	  		cout << quantity * 1.2 << endl;
	  	}else if(product == "sweets"){
	  		cout << quantity * 1.45 << endl;
	  	}else if(product == "peanuts"){
	  		cout << quantity * 1.6 << endl;
	  	}
	}
	else if(town == "Varna"){
		if (product == "coffee"){
	    	cout << quantity * 0.45 << endl;
	  	}else if(product == "water"){
	  		cout << quantity * 0.7 << endl;
	  	}else if(product == "beer"){
	  		cout << quantity * 1.1 << endl;
	  	}else if(product == "sweets"){
	  		cout << quantity * 1.35 << endl;
	  	}else if(product == "peanuts"){
	  		cout << quantity * 1.55 << endl;
	  	}
	}
	else if(town == "Plovdiv"){
		if (product == "coffee"){
	    	cout << quantity * 0.4 << endl;
	  	}else if(product == "water"){
	  		cout << quantity * 0.7 << endl;
	  	}else if(product == "beer"){
	  		cout << quantity * 1.15 << endl;
	  	}else if(product == "sweets"){
	  		cout << quantity * 1.30 << endl;
	  	}else if(product == "peanuts"){
	  		cout << quantity * 1.5 << endl;
	  	}
	}
}
