/*
	Напишете програма, която отпечатва класа на животното според неговото име, въведено от потребителя.
	•	dog -> mammal
	•	crocodile, tortoise, snake -> reptile
	•	others -> unknown
*/

#include <iostream>
#include <string>

using namespace std;

int main(){
	string animal;
	cin >> animal;

	if(animal == "dog"){
		cout << "mammal" << endl;
	}else if(animal == "crocodile" || animal == "tortoise" || animal == "snake"){
		cout << "reptile" << endl;
	}else{
		cout << "unknown" << endl;
	}
	
	return 0;
}