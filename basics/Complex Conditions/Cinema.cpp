/*
	В една кинозала столовете са наредени в правоъгълна форма в r реда и c колони. Има три вида прожекции с билети на различни цени:
	•	Premiere – премиерна прожекция, на цена 12.00 лева.
	•	Normal – стандартна прожекция, на цена 7.50 лева.
	•	Discount – прожекция за деца, ученици и студенти на намалена цена от 5.00 лева.
	Напишете програма, която чете тип прожекция (стринг), брой редове и брой колони в залата (цели числа), 
	въведени от потребителя, и изчислява общите приходи от билети при пълна зала. 
	Резултатът да се отпечата във формат като в примерите по-долу, с 2 знака след десетичната точка.
*/

#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

int main(){

	string projection;

	int rows, cols;
	cin >> projection >> rows >> cols;

	double sum;

	if(projection == "Premiere"){
		sum = rows * cols * 12;
		cout << fixed << setprecision(2) << sum << endl;
	}else if(projection == "Normal"){
		sum = rows * cols * 7.5;
		cout << fixed << setprecision(2) << sum << endl;
	}else if(projection == "Discount"){
		sum = rows * cols * 5;
		cout << fixed << setprecision(2) << sum << endl;
	}

	return 0;
}