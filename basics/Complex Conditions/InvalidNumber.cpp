/*
	Дадено число е валидно, ако е в диапазона [100…200] или е 0. 
	Да се напише програма, която чете цяло число, въведено от потребителя, и печата "invalid" ако въведеното число не е валидно.
*/

#include <iostream>
using namespace std;

int main(){

	int num;
	cin >> num;

	if(num < 100 || num > 200){
		cout << "invalid" << endl;
	}

	return 0;

	/*if((num <= 100 && num >= 200) || num == 0){
	  	cout << "invalid" << endl;
	}*/

	return 0;
}