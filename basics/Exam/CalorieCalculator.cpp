#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>

using namespace std;

int main(){
	char gender;
	double weight;
	double height;
	int age;
	string physics;

	cin >> gender >> weight >> height >> age;
	cin.ignore(256, '\n');
	getline(cin, physics);

	double calories;
	map<string, double>activity;
	activity["sedentary"] = 1.2;
	activity["lightly active"] = 1.375;
	activity["moderately active"] = 1.55;
	activity["very active"] = 1.725;

	double BNM = 0;

	if(gender == 'm'){
		BNM = 66 + (13.7 * weight) + (5 * height * 100) - (6.8 * age);
	}else{
		BNM = 655 + (9.6 * weight) + (1.8 * height * 100) - (4.7 * age);
	}
	

	/*cout << "weight : " << (13.7 * weight) << endl;
	cout << "height : " << (5 * height * 100) << endl;
	cout << "age : " << (6.8 * age) << endl;*/

	// cout << BNM << endl;
	calories = BNM * activity[physics];

	// cout << ceil(calories) << endl;


	cout << "To maintain your current weight you will need " 
	<< ceil(calories)
	<< " calories per day." << endl;
	return 0;
}