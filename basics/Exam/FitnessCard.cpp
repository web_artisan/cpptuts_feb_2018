#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>

using namespace std;

int main(){
	double sum;
	char gender;
	int age;
	string sport;

	cin >> sum >> gender >> age >> sport;

	map<string, int>maleSports;
	maleSports["Gym"] = 42;
	maleSports["Boxing"] = 41;
	maleSports["Yoga"] = 45;
	maleSports["Zumba"] = 34;
	maleSports["Dances"] = 51;
	maleSports["Pilates"] = 39;

	map<string, int>femaleSports;
	femaleSports["Gym"] = 35;
	femaleSports["Boxing"] = 37;
	femaleSports["Yoga"] = 42;
	femaleSports["Zumba"] = 31;
	femaleSports["Dances"] = 53;
	femaleSports["Pilates"] = 37;


	double discount = 0;

	if(age <= 19){
		discount = 0.20;
	}

	double totalPrice = 0;

	if(gender == 'm'){
		totalPrice = maleSports[sport] - (discount * maleSports[sport]);
		/*cout << "male discount : " << discount * maleSports[sport] << endl;
		cout << "male totalPrice : " << maleSports[sport] - (discount * maleSports[sport]) << endl;*/
		// cout << totalPrice << endl;
	}else{
		totalPrice = femaleSports[sport] - (discount * femaleSports[sport]);
		/*cout << "female discount : " << discount * femaleSports[sport] << endl;
		cout << "female totalPrice : " << femaleSports[sport] - (discount * femaleSports[sport]) << endl;*/
	}

	if(sum >= totalPrice){
		cout << "You purchased a 1 month pass for " << sport << "." << endl;
	}else{
		float diff = totalPrice - sum;
		cout << "You don't have enough money! You need $" << fixed << setprecision(2) << diff << " more." << endl;
	}

	
	return 0;
}