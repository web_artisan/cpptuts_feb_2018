#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>

using namespace std;

int main(){
	int days;

	double totalDistance = 0;
	int totalMinutes = 0;
	int totalCals = 0;

	int calPerMinute = 20;

	cin >> days;

	for (int i = 0; i < days; ++i){
		int minutes;
		double distance;
		string metric;

		cin >> minutes >> distance >> metric;

		double realDistance = 0;

		if(metric == "km"){
			realDistance = distance * 1000;
		}else{
			realDistance = distance;
		}

		totalDistance += realDistance;
		totalMinutes += minutes;
		totalCals += minutes * 20;
	}

	cout << "He ran " << fixed << setprecision(2) << totalDistance / 1000 << "km for "<< totalMinutes << " minutes and burned " << 
	totalCals << " calories." << endl;

	return 0;
}