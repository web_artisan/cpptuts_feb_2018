#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>

using namespace std;

int main(){
	double food,souv,hotel;
	cin >> food >> souv >> hotel;

	double gas = 420.0 / 100 * 7;

	double gasPrice = gas * 1.85;
	double foodAndSouvPrice = (food * 3) + (souv * 3);

	double hotelPriceFirstDay = hotel * 0.9;
	double hotelPriceSecondDay = hotel * 0.85;
	double hotelPriceLastDay = hotel * 0.8;

	// cout << foodAndSouvPrice << endl;

	double allExpences = gasPrice + foodAndSouvPrice + hotelPriceFirstDay + hotelPriceSecondDay + hotelPriceLastDay;

	cout << "Money needed: " /*<< setprecision(2)*/ << allExpences << endl;

	return 0;
}