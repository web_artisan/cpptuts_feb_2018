#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>

using namespace std;

int main(){
	int n;
	cin >> n;

	int stars = 2;
	int emptySpaces = (n * 2) - 2;

	for(int i = 1; i <= n; i++) {
		cout << string(emptySpaces, ' ') 
		<< string(stars , '*') << endl;

		stars += 2;
		emptySpaces = (n * 2) - stars;
	}

	cout << string((n * 2) - 2, '*') << endl;
	cout << string((n * 2) - 4, '*') << endl;
	cout << string((n * 2) - 2, '*') << endl;

	cout << string((n * 2), '*') << endl;

	cout << string(2, ' ') << string((n * 2) - 2, '*') << endl;
	cout << string(4, ' ') << string((n * 2) - 4, '*') << endl;
	cout << string(2, ' ') << string((n * 2) - 2, '*') << endl;

	int stars2 = n * 2;
	int emptySpaces2 = 0;

	for(int i = 1; i <= n; i++) {
		cout << string(stars2 , '*') << string(emptySpaces2, ' ') << endl;

		stars2 -= 2;
		emptySpaces2 = (n * 2) + stars;
	}

	return 0;
}