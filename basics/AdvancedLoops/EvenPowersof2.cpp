/*
	Да се напише програма, която чете число n, въведено от потребителя, и печата четните степени на 2 ≤ 2n: 20, 22, 24, 28, …, 2n. Примери:
вход	изход		вход	изход		вход	изход		вход	изход		вход	изход
3	1
4		4	1
4
16		5	1
4
16		6	1
4
16
64		7	1
4
16
64

*/

#include <iostream>
#include <cmath>
using namespace std;

int main(){
	int n;  
	cin >> n; 
	int num = 1;

	for (int i = 0; i <= n; i+=2) {
	  	cout << num << endl;
	  	num = num * 4;
	}


	return 0;
}