/*
	Напишете програма, която чете цяло положително число n, въведено от потребителя, и печата числата от n до 1 в обратен ред (от най-голямото към най-малкото). 
*/

#include <iostream>

using namespace std;

int main(){
	int n;
	cin >> n;

	for(int i = n; i >= 1; i--){
		cout << i << endl;
	}
	return 0;
}