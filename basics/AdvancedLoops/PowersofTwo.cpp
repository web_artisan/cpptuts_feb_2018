/*
	Напишете програма, която чете от конзолата цяло число n, въведено от потребителя, и печата числата от 1 до 2n. 
*/

#include <iostream>
#include <cmath>
using namespace std;

int main(){
	int n;  cin >> n; int num = 1;
	for (int i = 0; i <= n; i++) {
	  	cout << num << endl;
	  	num = num * 2;
	}


	return 0;
}