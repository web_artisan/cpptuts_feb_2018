/*
	Напишете програма, която: 
	Прочита цяло число n
	Отпечатва всички числа ≤ n от редицата: 1, 3, 7, 15, 31, …
	Всяко следващо число e равно на предишното * 2 + 1

	1, (1*2)+1 = 3, (3*2)+1 = 7, (7*2)+1 = 15
*/

#include <iostream>

using namespace std;

int main(){
	int n;
	cin >> n;
	int num = 1;
	
	while(num <= n){
  		cout << num << endl;
  		num = 2 * num + 1;
	}

	return 0;
}