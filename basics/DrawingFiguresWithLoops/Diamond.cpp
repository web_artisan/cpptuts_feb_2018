/*
	Напишете програма, която чете цяло число n (1 ≤ n ≤ 100), въведено от потребителя, и печата диамант с размер n като в примерите по-долу:
*/

#include <iostream>

using namespace std;

int main(){
	int n;
	cin >> n;

	if(n >= 1 && n <= 100) {

		int leftRight = (n - 1) / 2;

		for (int i = 1; i <= (n - 1) / 2; i++) {
		  	// Draw the top part
		  	cout << string(leftRight, '-');
		  	cout << '*';
		  	int mid = n - 2 * leftRight - 2;
		  	if (mid >= 0) {
		    	cout << string(mid, '-');
		    	cout << '*';
		  	}
		  	cout << string(leftRight, '-') << endl;
		  	leftRight--;
		}

		// cout << "*" << string(n - 2, '-') << "*" << endl;

		for (int i = 0; i <= (n - 1) / 2; ++i){
			cout << string(leftRight, '-');
		  	cout << '*';
		  	int mid = n - 2 * leftRight - 2;
		  	if (mid >= 0) {
		    	cout << string(mid, '-');
		    	cout << '*';
		  	}
		  	cout << string(leftRight, '-') << endl;
		  	leftRight++;
		}
	}
	return 0;
}