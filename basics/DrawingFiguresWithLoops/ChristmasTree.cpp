/*
	Напишете програма, която чете число n (1 ≤ n ≤ 100), въведено от потребителя, и печата коледна елха с размер n като в примерите по-долу:
*/

#include <iostream>

using namespace std;

int main(){
	int n;
	cin >> n;
	int x = 1;
	int emptySpaces = n - 1;
	cout << string(n + 1 , ' ') << "|" << endl;
	for (int i = 0; i < n; ++i){
		cout << string(emptySpaces, ' ') << string(x, '*') << " | " << string(x, '*') << string(emptySpaces, ' ') << endl;
		x++;
		emptySpaces--;
	}
	return 0;
}