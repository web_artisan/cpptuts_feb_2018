#include <iostream>
using namespace std;

int main(){
	int n;

	cin >> n;
	for (int row = 1; row <= n; row++)
	{
	  	for (int col = 1; col <= n - row; col++) {
	    	cout << " ";
	  	}
	  	cout << "*";
	  	for (int col = 1; col < row; col++) {
	    	cout << " *";
	  	}
	  	cout << endl;
	}

	int spaces = 1;
    int stars = n - spaces;
    for (int row = n; row < 2*n; row++){
       
        for (int col = 0; col < spaces; col++){
            cout << " ";
        }
        for (int col = 0; col < stars; col++){
            cout << "* ";
        }
        spaces++;
        stars--;
        cout << endl;;
    }
}
