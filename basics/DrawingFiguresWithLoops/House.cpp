/*
	Напишете програма, която чете число n (2 ≤ n ≤ 100), въведено от потребителя, и печата къщичка с размер n x n:
*/

#include <iostream>

using namespace std;

int main(){
	int n;
	cin >> n;

	//roof
	int roofTiles = 1;
	int flatTiles = (n + 1) / 2;
	if(n % 2 == 0){
		roofTiles = 2;
		for (int i = 0; i < (n + 1) / 2; ++i){
			flatTiles--;
			cout << string(flatTiles, '-') << string(roofTiles, '*') << string(flatTiles, '-') << endl;
			roofTiles += 2;
		}
	}else{
		for (int i = 0; i < (n + 1) / 2; ++i){
			flatTiles--;
			cout << string(flatTiles, '-') << string(roofTiles, '*') << string(flatTiles, '-') << endl;
			roofTiles += 2;
		}
	}

	//body
	for (int i = 0; i < n / 2; ++i){
		cout << "|" << string(n - 2, '*') << "|" << endl;
	}

	/*int stars = 1;
	if (n % 2 == 0) stars++;

	for (int i = 0; i < (n+1) / 2; i++){
		  // Draw the roof
		  int padding = (n - stars) / 2;
		  cout << string(padding, '-');
		  cout << string(stars, '*');
		  cout << string(padding, '-') << endl;
		  stars = stars + 2;
	}

	for (int i = 0; i < n / 2; i++){
	 	cout << "|" << string(n - 2, '*') << "|" << endl;
	}*/


	return 0;
}