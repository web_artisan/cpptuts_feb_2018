/*
Напишете програма, която чете цяло число n (3 ≤ n ≤ 100), въведено от потребителя, и печата слънчеви очила с размер 5*n x n като в примерите:
******   ******
*////*|||*////*


/******   ******

*/

#include <iostream>

using namespace std;

int main(){
	int n;
	cin >> n;

	cout << string(n * 2, '*') << string(n, ' ') << string(n * 2, '*') << endl;

	int rows = n / 2;
	int fullRows = n;

	if(rows > 0){
		fullRows -= rows;
	}

	for (int i = 0; i < n - 2 ; ++i){		
		if(i == (n-1) / 2 - 1){
			cout << "*" << string((n * 2) - 2 , '/') << "*" << string(n, '|') << "*" << string((n * 2) - 2, '/') << "*" << endl;	
		}else{
			cout << "*" << string((n * 2) - 2 , '/') << "*" << string(n, ' ') << "*" << string((n * 2) - 2, '/') << "*" << endl;
		}
		
	}

	cout << string(n * 2, '*') << string(n, ' ') << string(n * 2, '*') << endl;
	return 0;
}