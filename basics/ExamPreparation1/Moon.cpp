/*
	Георги е космонавт и следващата му мисия е да отиде до Луната. При положение, че се движи със скорост от
	X километра в час, той ще стигне до там за N часа. Приемаме, че разстоянието между Луната и Земята е
	384 400км. На Луната Георги ще прекара 3 часа, след което ще тръгне обратно за Земята.
	Напишете програма, която пресмята за колко часа Георги ще отиде и ще се върне и колко литра гориво ще
	са му нужни.
*/

#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
// #include <cmath>
#include <map>
#include <string>

using namespace std;

int main(){
	int length = 384400;
	double speed;
	double fuel100;
	double time;

	cin >> speed >> fuel100;

	double fuelAll = (fuel100 / 100) * (length * 2);

	time = (length / speed) * 2 + 3;

	cout << ceil(time) << endl;
	cout << fuelAll << endl;
	return 0;
}