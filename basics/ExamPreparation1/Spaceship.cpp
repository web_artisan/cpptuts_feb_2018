/*
	Георги трябва да построи космически кораб, който да събира част от екипажа му. За целта, той трябва да го
направи така, че да има място за поне трима астронавти, но за не повече от 10. Всеки астронавт се нуждае от
малка стая, която да е с размери: 2 метра ширина, 2 метра дължина и с 40 см по-висока от средната
височина на астронавтите.
Напишете програма, която изчислява обема на кораба, колко астронавта ще събере и принтира на
конзолата дали той е достатъчно голям.
*/

#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>

using namespace std;

int main(){
	double width;
	double length;
	double height;
	double asHeight;

	double spaceshipVolume;
	double singleRoomVolume;
	double availableRooms;

	cin >> width >> length >> height >> asHeight;

	spaceshipVolume = width * height * length;
	singleRoomVolume = 4 * (asHeight + 0.40);
	availableRooms = spaceshipVolume / singleRoomVolume;

	int asses = floor(availableRooms);

	if(asses >= 3 && asses <= 10){
		cout << "The spacecraft holds " << asses << " astronauts." << endl;
	}else if(asses < 3){
		cout << "The spacecraft is too small." << endl;
	}else if(asses > 10){
		cout << "The spacecraft is too big." << endl;
	}
	return 0;
}